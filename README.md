# EMX (Energy Management eXperience)

## Overview

EMX is a frontend application built with React for the Energy Management System. It provides a user-friendly interface to monitor and manage energy-related data.

## Getting Started

### Prerequisites

Make sure you have Node.js and npm installed on your machine.

- Node.js: [Download and Install Node.js](https://nodejs.org/)
- npm: [Download and Install npm](https://www.npmjs.com/get-npm)

### Installation

1. Clone the EMX repository to your local machine.

    ```
    git clone https://gitlab.com/CSavu/emx.git
    ```

2. Navigate to the project directory.

    ```
    cd emx
    ```

3. Install dependencies.

    ```
    npm install
    ```

### Running Locally

To start the application locally, use the following command:

```
npm run start:local
```
