import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import { useAuth } from '../context/AuthContext';
import { getAllDevicesForUser } from '../services/device-management-service';

function ListItemLink(props) {
    return <ListItem button component={Link} {...props} />;
}

function Profile() {
    const { isUserLoggedIn, refreshAuthorization, authorizedUserId, isUserAdmin, isAuthInProgress } = useAuth();
    //const { user } = {};
    const [devices, setDevices] = useState([]);
    const [userId, setUserId] = useState(null);

    useEffect(() => {
        refreshAuthorization();
    }, []);

    useEffect(() => {
        setUserId(authorizedUserId);
    }, [authorizedUserId]);

    useEffect(() => {
        console.log(isUserAdmin);
    }, [isUserAdmin]);

    useEffect(() => {
        async function getAllDevices() {
            const devicesResponse = await getAllDevicesForUser(userId);
            setDevices(devicesResponse);
            console.log(devicesResponse);
        }

        if (userId) getAllDevices();
    }, [userId]);

    useEffect(() => {
        console.log(isUserLoggedIn);
    }, [devices]);

    if (!isAuthInProgress && isUserLoggedIn && !isUserAdmin) {
        return (
            <section id="profile">
                <div className="container">
                    <h2>Assigned devices</h2>
                    <List>
                        {
                            devices?.map((device, index) => {
                                return (device.name && (<div key={index}>
                                    <ListItem>
                                        <ListItemLink>
                                            <ListItemText
                                                className="list-item-text">{device.name} </ListItemText>

                                            <ListItemText
                                                className="list-item-text">Address: {device.address} </ListItemText>
                                        </ListItemLink>
                                    </ListItem>
                                    <Divider/>
                                </div>
                                ));
                            })}
                    </List>
                </div>
            </section>
        );
    } else if (!isAuthInProgress) {
        window.location.href = '/login';
        return null;
    }
}

export default Profile;
