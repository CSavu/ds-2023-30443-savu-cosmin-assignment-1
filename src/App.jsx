import React from 'react';
import Header from './containers/Header';
import Profile from './pages/Profile';
import Home from './pages/Home';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import Login from './pages/Login';
import './styles.css';
import UserDashboard from './pages/UserDashboard';
import DeviceDashboard from './pages/DeviceDashboard';

function App() {

    return (
        <Router>
            <Header />
            <Routes>
                <Route exact path="/profile" element={<Profile />} />
                <Route exact path="/login" element={<Login />} />
                <Route exact path="/users" element={<UserDashboard />} />
                <Route exact path="/devices" element={<DeviceDashboard />} />
                <Route exact path="/" element={<Home />} />
            </Routes>
        </Router>
    );
}

export default App;
