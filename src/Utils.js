import { verifyToken } from './services/authorization-service';

export function getCookie(name) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length >= 2) return parts.pop().split(';').shift();
}

export async function isUserLoggedIn() {
    const isTokenValid = await verifyToken();
    return !isTokenValid;
}

export function getIdFromUrl(url) {
    return url.substring(url.lastIndexOf('/') + 1);
}