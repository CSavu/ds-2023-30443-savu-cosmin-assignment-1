import React, { createContext, useContext, useReducer } from 'react';
import { verifyToken } from '../services/authorization-service';

// Action types
const LOGOUT = 'LOGOUT';
const LOGIN = 'LOGIN';
const REFRESH = 'REFRESH';

// Reducer function
const authReducer = (state, action) => {
    switch (action.type) {
    case REFRESH:
        return { 
            ...state,
            isUserLoggedIn: action.payload?.tokenValid, 
            isUserAdmin: action.payload?.role === 'ADMIN', 
            authorizedUserId: action.payload?.userId,
            isAuthInProgress: false
        };
    case LOGIN:
        return { ...state, isUserLoggedIn: true, isAuthInProgress: false };
    case LOGOUT:
        document.cookie = 'token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;';
        return { ...state, isUserLoggedIn: false };
    default:
        return state;
    }
};

// Initial state
const initialState = {
    isUserLoggedIn: false,
    isUserAdmin: false,
    authorizedUserId: null,
    isAuthInProgress: true
};

const getAuthorization = async () => {
    console.log('Calling verifyToken');
    return await verifyToken();
};

// Create context
const AuthContext = createContext();

// Context provider component
export const AuthProvider = ({ children }) => {
    const [state, dispatch] = useReducer(authReducer, initialState);

    // Actions
    const logout = () => dispatch({ type: LOGOUT });
    const login = () => dispatch({ type: LOGIN });
    const refreshAuthorization = async () => {
        const authorizationResponse = await getAuthorization();
        dispatch({ type: REFRESH, payload: authorizationResponse });
    };

    // Provide the context value
    const contextValue = {
        isUserLoggedIn: state.isUserLoggedIn,
        isUserAdmin: state.isUserAdmin,
        authorizedUserId: state.authorizedUserId,
        isAuthInProgress: state.isAuthInProgress,
        logout,
        login,
        refreshAuthorization
    };

    return <AuthContext.Provider value={contextValue}>{children}</AuthContext.Provider>;
};

// Custom hook to use the AuthContext
export const useAuth = () => useContext(AuthContext);