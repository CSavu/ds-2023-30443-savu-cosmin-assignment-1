export const authenticateUser = async (username, password) => {
    try {
        const response = await fetch(`https://${process.env.REACT_APP_API_HOST}:8443/api/authentication/authenticate`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ username, password })
        });

        const data = await response.json();
        return data;
    } catch (error) {
        console.error(error);
    }
};
