import { getCookie } from '../Utils';

export const verifyToken = async () => {
    try {
        console.log(getCookie('token'));
        const response = await fetch(`https://${process.env.REACT_APP_API_HOST}:8443/api/authorization/verifyToken`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + getCookie('token'),
            }
        });

        const data = await response.json();
        return data;
    } catch (error) {
        console.error(error);
    }
};